# Bornera 

Se trata de una barra multicontacto para distribuir un voltaje de entrada a 8 lineas de salida, cada una
de ellas protegida con un fusible de cristal.

![Esquemático](img/bornera3.png) 

Las salidas son del tipo 
[borne KF301](https://www.lcsc.com/product-detail/Screw-terminal_Cixi-Kefa-Elec-KF301-5-0-2P_C474881.html)  
de dos pines con paso de 5mm, ideal para cables con calibre 14 a 22 AWG

En el jack de entrada cuenta con un diodo de protección contra polarización inversa.

# Vista Frontal

![Vista frontal](img/bornera1.png)

# Vista trasera

![Vista trasera](img/bornera2.png)

# Directorios

La intención de este proyecto es que puedas reproducirlo de forma sencilla, por ello compartimos 
tanto los archivos del Proyecto [KiCAD](https://kicad.org) como los pdf listos para la termotransferencia. 

## Kicad

[Archivos KiCAD](kicad)

## PDF
[Archivos PDF](pdf)

## IMG
[Imágenes](img)
